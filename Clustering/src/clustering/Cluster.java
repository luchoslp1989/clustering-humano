package clustering;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import grafos.AGM;
import grafos.BFS;
import grafos.GrafoPesado;

public class Cluster {
	private Map<String, Integer> similaridades;

	private GrafoPesado grafo;
	private ArrayList<Persona> personas;

	
	public Cluster(ArrayList<Persona> personas) {
		if(personas.size()==0)
			throw new IllegalArgumentException("No puede obtener cluster con una lista vacia!");
		similaridades = new HashMap<String, Integer>();
		this.personas = personas;
		this.grafo = new GrafoPesado(personas.size());
		agregarAristaConPeso(personas);
		this.generarCluster();
	}

	// Calcula la similaridad entre dos personas
	private int calcularSimilaridad(int[] interesesPersonas1, int[] interesesPersona2) {
		int similaridad = 0;
		for (int i = 0; i < interesesPersonas1.length; i++) {
			similaridad += Math.abs(interesesPersonas1[i] - interesesPersona2[i]);

		}
		// System.out.println(similaridad);
		return similaridad;
	}

	// Agregar una arista con peso a un grafo.
	public void agregarAristaConPeso(List<Persona> personas) {
		int peso = 0;

		for (int i = 0; i < personas.size(); i++)
			for (int j = 0; j < personas.size(); j++)
				if (i != j) {
					peso = calcularSimilaridad(personas.get(i).getIntereses(), personas.get(j).getIntereses());
					guardarSimilaridad(i, j, peso);
					// similaridades.put("", peso);
					grafo.agregarArista(i, j, peso);
				}

	}

	// Guarda la simililaridad entre dos personas para ser mostrada en la interfaz.
	private void guardarSimilaridad(int indicePrimerPersona, int indiceSegundaPersona, int peso) {
		String nombreDeLaRelacion = "";
		nombreDeLaRelacion = personas.get(indicePrimerPersona).getNombre() + "-"
				+ personas.get(indiceSegundaPersona).getNombre();
		similaridades.put(nombreDeLaRelacion, peso);

	}

	public Map<String, Integer> obtenerSimilidadEntreDosPersonas() {
		return similaridades;
	}
	
	
	public void generarCluster() {
		AGM agm = new AGM();
		GrafoPesado obtenido = agm.arbolGenerador(this.grafo);
		this.grafo = obtenido;
		if (this.grafo.tamano()>1) {
			this.grafo.eliminarAristaMayor();
		}
		
	}

	public Set<Persona> obtenerCluster() {
		Set<Integer> indices = BFS.alcanzables(grafo, 0);
		Set<Persona> primerGrupo = new HashSet<Persona>();
		for (Integer idx : indices) {
			primerGrupo.add(personas.get(idx));
		}
		return primerGrupo;
	}

	public Set<Persona> obtenerPrimerGrupo() {
		return obtenerCluster();
	}

	public Set<Persona> obtenerSegundoGrupo() {
		Set<Persona> segundoGrupo = new HashSet<Persona>();
		segundoGrupo.addAll(personas);
		segundoGrupo.removeAll(obtenerPrimerGrupo());
		return segundoGrupo;
	}
	
}
