package clustering;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

public class PersonaTest {

	
	
	@Test
	public void personasIgualesTest()
	{
		Persona a= new Persona("A", 2, 3, 1, 1); //vertice 0
		Persona b= new Persona("A", 2, 3, 1, 1); //vertice 1
		assertTrue(a.iguales(b));
	}
	
	@Test
	public void personasDistintasTest()
	{
		Persona a= new Persona("A", 2, 3, 1, 1); //vertice 0
		Persona b= new Persona("A", 2, 3, 5, 1); //vertice 1
		assertFalse(a.iguales(b));
	}
	
}


