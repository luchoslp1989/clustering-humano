package clustering;

import java.util.Arrays;

public class Persona {
	private String nombre;
	private int[] intereses;

	public Persona(String nombre, int interesDeporte, int interesMusica, int interesEspectaculo, int interesCiencia) {
		this.nombre = nombre;
		intereses = new int[] { interesDeporte, interesMusica, interesEspectaculo, interesCiencia };
	}

	public String getNombre() {
		return this.nombre;
	}

	public int[] getIntereses() {
		return intereses;
	}



	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", intereses=" + Arrays.toString(intereses) + "\n";
	}
	

	public Boolean iguales(Persona p)
	{
		return this.getNombre()==p.getNombre() && this.igualIntereses(p);	
	}
	
	private boolean igualIntereses(Persona p)
	{
		//boolean iguales=false;
		for (int i = 0; i < intereses.length; i++) {
			if(intereses[i] != p.intereses[i])
				return false;
			
		}
		return true;
	}
	


}
