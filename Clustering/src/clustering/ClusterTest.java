package clustering;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

import grafos.Assert;

public class ClusterTest {
	
	
	@Test ( expected = IllegalArgumentException.class)
	public void obtenerConListaVaciaTest()
	{
		ArrayList<Persona> personas= new ArrayList<Persona>();
		Cluster cluster=new Cluster(personas);
	}

	@Test
	public void obtenerPrimerGrupoTest()
	{
		Persona a= new Persona("A", 2, 3, 1, 5); //vertice 0
		Persona b= new Persona("B", 5, 5, 1, 1); //vertice 1
		Persona c= new Persona("C", 5, 5, 1, 1); //vertice 2
		Persona d= new Persona("D", 3, 5, 4, 2); //vertice 3
		Persona e= new Persona("E", 3, 3, 3, 3); //vertice 4
		ArrayList<Persona> personas= new ArrayList<Persona>();
		personas.add(a); personas.add(b); personas.add(c); personas.add(d); personas.add(e); 
		Cluster cluster=new Cluster(personas);
		Set<Persona> obtenido=cluster.obtenerPrimerGrupo();
		Persona[] esperado = {a,e,d};
		Assert.Iguales(esperado, obtenido);
		
	}
	
	@Test
	public void obtenerSegundoGrupoTest()
	{
		Persona a= new Persona("A", 2, 3, 1, 5); //vertice 0
		Persona b= new Persona("B", 5, 5, 1, 1); //vertice 1
		Persona c= new Persona("C", 5, 5, 1, 1); //vertice 2
		Persona d= new Persona("D", 3, 5, 4, 2); //vertice 3
		Persona e= new Persona("E", 3, 3, 3, 3); //vertice 4
		ArrayList<Persona> personas= new ArrayList<Persona>();
		personas.add(a); personas.add(b); personas.add(c); personas.add(d); personas.add(e); 
		Cluster cluster=new Cluster(personas);
		Set<Persona> obtenido=cluster.obtenerSegundoGrupo();
		Persona[] esperado = {c,b};
		Assert.Iguales(esperado, obtenido);
		
	}
	
	@Test
	public void similaridadesTest()
	{
		Persona a= new Persona("B", 5, 5, 1, 1); 
		Persona b= new Persona("C", 5, 5, 1, 1); 
		ArrayList<Persona> personas= new ArrayList<Persona>();
		personas.add(a); personas.add(b);
		Cluster cluster=new Cluster(personas);
		Map<String, Integer> obtenido = cluster.obtenerSimilidadEntreDosPersonas();
		Map<String, Integer> esperado = new HashMap<String, Integer>();
		String nombreDeLaRelacion = b.getNombre() + "-"
				+ a.getNombre();
		esperado.put(nombreDeLaRelacion, 0);
		assertTrue(obtenido.containsKey(nombreDeLaRelacion) && obtenido.containsValue(0));
	}
	
		
	
	
	
}
