package interfaz;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

import clustering.Cluster;
import clustering.Persona;

public class Interfaz {

	private JFrame frame;
	private JTable table;
	JList primerGrupoLista;
	JList segundoGrupoLista;
	JList similaridadLista;
	private String nombre;
	private ArrayList<Persona> personas = new ArrayList<Persona>();
	private Cluster cluster;
	Set<Persona> grupo1;
	Set<Persona> grupo2;
	Boolean SeAgregoPersona = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					Interfaz window = new Interfaz();
					window.frame.setVisible(true);
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "serial", "unchecked" })
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 655, 423);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(27, 11, 430, 132);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Persona", "Deportes", "M\u00FAsica", "Espect\u00E1culos", "Ciencia" }) {
			boolean[] columnEditables = new boolean[] { false, false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setResizable(false);
		scrollPane.setViewportView(table);

		// Boton que permite agregar una persona a la tabla.
		JButton agregarPersonaBtn = new JButton("Agregar persona");
		agregarPersonaBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				agregarPersona();
				SeAgregoPersona = true;
			}
		});
		// Bot�n que permite calcular los grupos segund similaridad.
		agregarPersonaBtn.setBounds(27, 154, 217, 23);
		frame.getContentPane().add(agregarPersonaBtn);

		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setBounds(27, 224, 217, 132);
		frame.getContentPane().add(scrollPane2);

		JScrollPane scrollPane3 = new JScrollPane();
		scrollPane3.setBounds(249, 224, 208, 132);
		frame.getContentPane().add(scrollPane3);
		JScrollPane scrollPane4 = new JScrollPane();
		scrollPane4.setBounds(467, 34, 162, 322);
		frame.getContentPane().add(scrollPane4);

		JButton mostrarGruposBtn = new JButton("Mostrar grupos");
		mostrarGruposBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (SeAgregoPersona) {
					ObtenerGrupos(personas);
					mostrarPrimerGrupo();
					mostrarSegundoGrupo();
					mostrarSimilaridad();

					scrollPane2.setViewportView(primerGrupoLista);

					scrollPane3.setViewportView(segundoGrupoLista);
					scrollPane4.setViewportView(similaridadLista);
				}
			}
		});
		mostrarGruposBtn.setBounds(254, 154, 203, 23);
		frame.getContentPane().add(mostrarGruposBtn);

		JLabel lblNewLabel = new JLabel("Grupo 1");
		lblNewLabel.setBounds(106, 199, 46, 14);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblGrupo = new JLabel("Grupo 2");
		lblGrupo.setBounds(330, 199, 46, 14);
		frame.getContentPane().add(lblGrupo);

		JLabel lblNewLabel_1 = new JLabel("Similaridad");
		lblNewLabel_1.setBounds(523, 14, 70, 14);
		frame.getContentPane().add(lblNewLabel_1);

	}

	public void mostrarPrimerGrupo() {
		primerGrupoLista = new JList();
		DefaultListModel modelo = new DefaultListModel();
		for (Persona persona : grupo1) {
			modelo.addElement(persona.getNombre());
		}
		primerGrupoLista.setModel(modelo);

	}

	public void mostrarSegundoGrupo() {
		segundoGrupoLista = new JList();
		DefaultListModel modelo = new DefaultListModel();
		for (Persona persona : grupo2) {
			modelo.addElement(persona.getNombre());
		}
		segundoGrupoLista.setModel(modelo);

	}

	private void mostrarSimilaridad() {
		similaridadLista = new JList();
		String elemento;
		DefaultListModel modelo = new DefaultListModel();
		Map<String, Integer> similaridades = cluster.obtenerSimilidadEntreDosPersonas();
		for (Entry<String, Integer> entry : similaridades.entrySet()) {
			elemento = entry.getKey() + " " + entry.getValue();
			//System.out.println("Elementos" + elemento);
			modelo.addElement(elemento);
		}
		similaridadLista.setModel(modelo);
	}

//	private void calcularIndicesDeSimilaridad() {
//		Map<String, Integer> similaridades = new HashMap<String, Integer>();
//		for (Entry<String, Integer> entry : similaridades.entrySet()) {
//			System.out.println("clave=" + entry.getKey() + ", valor=" + entry.getValue());
//		}
//	}

	private void agregarPersona() {

		JOptionPane Ventana = new JOptionPane();
		nombre = String.valueOf(JOptionPane.showInputDialog(null,"Ingrese su nombre"));
		
		 
		 
	if (!nombre.equals("") && !nombre.equals("null")) {
		
		Boolean Salir=false;
		Object[] Gradointeres= { 1,2,3,4,5};
		String Interes[]= {"Deporte","Musica","Espectaculo","Ciencia"};		
		int respuestas[]= new int [Interes.length];
			
		  for (int i =0; i<Interes.length ;i++) {
			  int opcion=JOptionPane.showOptionDialog(null, "Elige su grado de interes sobre= " + Interes[i], "",
              JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, Gradointeres,null);
		 
			  respuestas[i]=opcion+1;
		
			 if (opcion==JOptionPane.CLOSED_OPTION) {
				 Salir=true;
				 break;
			 }
		  
		  }
		  
		if (Salir==false) {
			Persona persona = new Persona(nombre, respuestas[0],
					respuestas[1], respuestas[2],respuestas[3]);

			DefaultTableModel model = (DefaultTableModel) table.getModel();
			model.addRow(new Object[] { persona.getNombre(), persona.getIntereses()[0], persona.getIntereses()[1],
					persona.getIntereses()[2], persona.getIntereses()[3] });

			personas.add(persona);
			
		}

		} else if (nombre.equals("null")) {
		}

		else {
			JOptionPane.showMessageDialog(Ventana, "Debe Ingresar su nombre para poder agregar a la tabla!");
		}
	
	}

	public void ObtenerGrupos(ArrayList<Persona> a) {

		cluster = new Cluster(a);
		grupo1 = cluster.obtenerPrimerGrupo();
		grupo2 = cluster.obtenerSegundoGrupo();

	}

	@Override
	public String toString() {
		return "Interfaz [table=" + this.table + "]";
	}

}
