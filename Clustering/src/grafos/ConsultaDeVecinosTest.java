package grafos;

import static org.junit.Assert.*;



import org.junit.Test;

public class ConsultaDeVecinosTest {

	@Test (expected = IllegalArgumentException.class)
	public void verticeNegativoTest() 
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.vecinos(-1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void verticeExcedidoTest() 
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.vecinos(5);
	}
	
	@Test
	public void todosAisladosTest() 
	{
		GrafoPesado grafo = new GrafoPesado(5);
		assertEquals(0, grafo.vecinos(2).size());
	}
	
	@Test
	public void verticeUniversalTest() 
	{
		GrafoPesado grafo = new GrafoPesado(4);
		grafo.agregarArista(1, 0, 1);
		grafo.agregarArista(1, 2, 1);
		grafo.agregarArista(1, 3, 1);
		
		int[] esperado = {0, 2, 3};
		Assert.Iguales (esperado, grafo.vecinos(1));
  }
	
	

	@Test
	public void verticeNormalTest()
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(1, 3, 1);
		grafo.agregarArista(2, 3, 1);
		grafo.agregarArista(2, 4, 1);
		
		int[] esperado = {1, 2};
		Assert.Iguales(esperado, grafo.vecinos(3));
		
	}
	
	
	
	
	

}
