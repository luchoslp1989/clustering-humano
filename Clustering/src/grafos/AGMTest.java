package grafos;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

public class AGMTest {

	@Test ( expected = IllegalArgumentException.class )
	public void agmDeGrafoNuloTest()
	{
		GrafoPesado grafoPeso= new GrafoPesado(0);
		AGM agm= new AGM();
		GrafoPesado obtenido=agm.arbolGenerador(grafoPeso);
	}
	

	
	@Test
	public void agmDeGrafoCompletoTest()
	{
		GrafoPesado grafo = inicializarGrafoCompleto();
		AGM agm= new AGM();
		GrafoPesado obtenido= agm.arbolGenerador(grafo);
		
		GrafoPesado esperado = new GrafoPesado(4);
		esperado.agregarArista(0, 1, 1);
		esperado.agregarArista(0, 2, 2);
		esperado.agregarArista(0, 3, 3);
		
		assertTrue(esperado.iguales(obtenido));
		
		
		
	}
	
	@Test
	public void agmDeGrafoCompleto3Test()
	{
		GrafoPesado grafo = new GrafoPesado(3);
		grafo.agregarArista(0, 1, 7);
		grafo.agregarArista(0, 2, 0);
		grafo.agregarArista(1, 2, 7);
		AGM agm= new AGM();
		GrafoPesado obtenido= agm.arbolGenerador(grafo);
		
		GrafoPesado esperado = new GrafoPesado(3);
		esperado.agregarArista(0, 2, 0);
		esperado.agregarArista(0, 1, 7);
		esperado.agregarArista(1, 2, 7);
		
		assertTrue(esperado.iguales(obtenido));
		
		
		
	}
	
	@Test
	public void eliminarAristaMAyor()
	{
		GrafoPesado grafo = inicializarGrafoCompleto();
		AGM agm= new AGM();
		GrafoPesado obtenido= agm.arbolGenerador(grafo);
		obtenido.eliminarAristaMayor();
		
		
		GrafoPesado esperado = new GrafoPesado(4);
		esperado.agregarArista(0, 1, 1);
		esperado.agregarArista(0, 2, 2);
		
		
		assertTrue(esperado.iguales(obtenido));
	}
	
	private GrafoPesado inicializarGrafoCompleto() 
	{
		GrafoPesado grafo = new GrafoPesado(4);
		grafo.agregarArista(0, 1, 1);
		grafo.agregarArista(0, 2, 2);
		grafo.agregarArista(0, 3, 3);
		grafo.agregarArista(1, 2, 4);
		grafo.agregarArista(1, 3, 5);
		grafo.agregarArista(2, 3, 6);
		
		return grafo;
	}

}
