package grafos;

import static org.junit.Assert.*;

import org.junit.Test;

public class EdicionDeAristaTest {
	
	@Test (expected = IllegalArgumentException.class)
	public void primerVerticeNegativoTest()
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(-1, 3, 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void primerVerticeExcedidoTest()
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(5, 2, 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void segundoVerticeNegativoTest()
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(2, -1, 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void segundoVerticeExcedidoTest()
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(2, 5, 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void agregarLoopTest()
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(2, 2, 1);
	}
	
	@Test
	public void agregarAristaDosVecesTest() 
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(2, 4, 1);
		grafo.agregarArista(2, 4, 1);
		assertTrue( grafo.pesoArista(2, 4)>=0 );
		
		
	}

	@Test
	public void eliminarAristaExistenteTest() 
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(2, 4, 1);
		grafo.eliminarArista(2, 4);
		assertFalse( grafo.pesoArista(2, 4) >= 0);
		
		
	}
	
	@Test
	public void eliminarAristaDosVecesTest() 
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(2, 4, 1);
		grafo.eliminarArista(2, 4);
		grafo.eliminarArista(2, 4);
		
		assertFalse( grafo.pesoArista(2, 4) >= 0);
		
		
	}
	
	@Test
	public void eliminarAristaInexistenteTest() 
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.eliminarArista(2, 4);
		assertFalse( grafo.pesoArista(2, 4) >= 0 );
		
		
	}
	
	@Test
	public void aristaExistenteTest() 
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(2, 3, 1);
		assertTrue( grafo.pesoArista(2, 3) >= 0);
		
		
	}
	
	@Test
	public void aristaOpuestaTest() 
	{
	
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(2, 3, 1);
		assertTrue( grafo.pesoArista(3, 2) >= 0 );
	
	}
	
	@Test
	public void aristaInexistenteTest() 
	{
	
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(2, 3, 1);
		assertFalse( grafo.pesoArista(1, 4) >= 0);
	
	}
	
	
	
	
	
	
	
}
