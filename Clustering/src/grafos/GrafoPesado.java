package grafos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class GrafoPesado 
{
	//representamos el grafo por su matriz de adyacencia
	private int[][] A;
	private int _vertice;
	ArrayList<Arista> aristas;

	
	
	//LA cantidad de vertices esta predeterminada por el constructor
	public GrafoPesado(int vertices) 
	{
		A = new int[vertices][vertices];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A.length; j++) {
				A[i][j]=-1;
				A[j][i]=-1;
			}
		}
		_vertice = vertices;
		this.aristas=new ArrayList<Arista>();
		
	}
	
	//agregado de aristas
	public void agregarArista(int i, int j, int peso)
	{
		verificarVertice(i);
		
		verificarVertice(j);
		
		verificarDistintos(i, j);
		
			
		A[i][j]=peso;
		A[j][i]=peso;
		aristas.add(new Arista(i, j, A[i][j]=peso));
	}
	
	//eliminacion de arista
		public void eliminarArista(int i, int j)
		{
			verificarVertice(i);
			
			verificarVertice(j);
			
			verificarDistintos(i, j);
				
			A[i][j]=-1;
			A[j][i]=-1;
			Arista a=new Arista(i, j, 0);
			aristas.remove(a);
		}

	

	
	
	//informa si existe arista especificada
	public int pesoArista(int i, int j)
	{
		verificarVertice(i);
		
		verificarVertice(j);
		
		verificarDistintos(i, j);
		
		return A[i][j];
	}
	
	public boolean existeArista(int i, int j)
	{
		verificarVertice(i);
		
		verificarVertice(j);
		
		verificarDistintos(i, j);
		
		return A[i][j]>=0;
	}
	
	
	//Cantidad de vertices
	public int tamano()
	{
		return A.length;
	}
	
	//vecinos de un vertice
	public Set<Integer> vecinos(int i)
	{
		verificarVertice(i);
		
		Set<Integer> ret = new HashSet<Integer>();
		for (int j = 0; j < this.tamano(); j++) if ( i!=j ) 
		{
			if( pesoArista(i, j)>0)
				ret.add(j); 
		}
		
		return ret;
	}
	
	
	//verifica que sea un vertice valido
	private void verificarVertice(int i) {
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i>= A.length )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}
	
	//Verifica que i y j sean distintos
	private void verificarDistintos(int i, int j) {
		if( i==j )
			throw new IllegalArgumentException("No se permiten loops: ( "+ i +", " + j + ")");
	}
	
	public int vertices()
	{
		return _vertice;
	}
	
	public Set<Arista> ObtenerAristas()
	{
		Set<Arista> ret = new HashSet<Arista>();
		for (int i = 0; i < this.tamano(); i++) {
			for (int j = 0; j < this.tamano(); j++) if(i!=j & this.pesoArista(i, j)>0)
			{
				
				ret.add(new Arista(i, j, this.pesoArista(i, j)));
			}
			
			
		}
		return ret;
	}
	
	public Boolean iguales(GrafoPesado g)
	{
		Boolean igual=false;
		//if(g.tamano()==1 && this.tamano()==1)	
		//	return true;
		for (int i = 0; i < this.tamano(); i++)
			for (int j = 0; j < this.tamano(); j++)
			{				
				if (i!=j)
				{
				igual = this.existeArista(i, j) && g.existeArista(i, j) || !(this.existeArista(i, j)) && !(g.existeArista(i, j));
				}
			}
		return igual;
			
		
	}
	
	public void eliminarAristaMayor()
	{
		Collections.sort(this.aristas);
		Arista a= this.aristas.get(0);
		this.eliminarArista(a.obtenerVertice1(), a.obtenerVertice2());
	}
	
	public void imprimirAristaOrdenada()
	{
		Collections.sort(this.aristas);
		for (Arista a : this.aristas) 
		{
			System.out.println(a.toString());
			
		}
	}
}
