package grafos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class BFS 
{
	private static ArrayList<Integer> L;
	private static boolean[] marcados;
	
	public static boolean esConexo(GrafoPesado g) 
	{
		if (g==null)
			throw new IllegalArgumentException("Se intento consultar con un grafo null!" );
		
		if (g.vertices() == 0)
			return true;
		
		return alcanzables(g, 0).size() == g.vertices();
	}

	public static Set<Integer> alcanzables(GrafoPesado g, int origen)
	{
		Set<Integer> marcados = new HashSet<Integer>();
		ArrayList<Integer> pendientes= new ArrayList<Integer>();
		pendientes.add(origen);
		
		while (pendientes.size() != 0)
		{
			int actual =	pendientes.get(0);
			marcados.add(actual);
			pendientes.remove(0);
			
			for (Integer v : g.vecinos(actual)) {
				if(marcados.contains(v) == false)
					pendientes.add(v);
			}

						
		}
		
		return marcados;
	}


}
