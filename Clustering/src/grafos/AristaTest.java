package grafos;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

public class AristaTest {

	@Test
	public void aristaDistintasTest()
	{
		Arista a = new Arista(0, 1, 2);
		Arista b = new Arista(0, 2, 2);
		assertFalse(a.equals(b));
				
	}
	
	@Test
	public void aristaIgualesTest()
	{
		Arista a = new Arista(0, 1, 2);
		Arista b = new Arista(0, 1, 2);
		assertTrue(a.equals(b));
				
	}
	
	@Test
	public void aristasDescendentesTest()
	{
		ArrayList<Arista> aristas=new ArrayList<Arista>();
		Arista a = new Arista(0, 1, 2);
		Arista b = new Arista(0, 4, 8);
		Arista c = new Arista(0, 3, 5);
		Arista d = new Arista(1, 5, 10);
		Arista e = new Arista(3, 6, 1);
		aristas.add(a); aristas.add(b); aristas.add(c); aristas.add(d); aristas.add(e);
		
		Collections.sort(aristas);
		Arista[] esperado= {d,b,c,a,e};
		Assert.ArrayIguales(esperado, aristas);
		
				
	}
	
	
	
	
}
