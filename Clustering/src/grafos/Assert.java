package grafos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Set;

import clustering.Persona;

public class Assert 
{
	public static  void Iguales(int[] esperado, Set<Integer> obtenido) 
	{	
		assertEquals(esperado.length, obtenido.size());
		
		for (int i = 0; i < esperado.length; i++) {
			assertTrue(obtenido.contains(esperado[i]));
		}
		
		
	}

	public static void ArrayIguales(Arista[] esperado, ArrayList<Arista> aristas) {
		for (int i = 0; i < esperado.length; i++) {
			assertTrue(esperado[i].equals(aristas.get(i)));
		}
			
		
		
	}
	
	public static  void Iguales(Persona[] esperado, Set<Persona> obtenido) 
	{	
		assertEquals(esperado.length, obtenido.size());
		
		for (Persona p : obtenido) {
			obtenido.contains(p);
		}
		
		
	}
	
}
