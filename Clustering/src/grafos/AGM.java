package grafos;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;



public class AGM 
{
	GrafoPesado agm;
	Set<Integer> amarillos;
	Arista aristaMinima;
	
	int iterador;
	public GrafoPesado arbolGenerador(GrafoPesado grafoPeso)
	{
		if(grafoPeso.tamano()==0)
			throw new IllegalArgumentException("La cantidad de vertices tiene que ser mayor a 0");
		if(grafoPeso.tamano()==1)
			return grafoPeso;
		agm=new GrafoPesado(grafoPeso.tamano());
		amarillos=new HashSet<Integer>();
		amarillos.add(0);
		iterador=1;
		while (this.iterador<grafoPeso.tamano()) {
			Arista aristaLiviana= obtenerAristaLiviana(grafoPeso, amarillos);
			agm.agregarArista(aristaLiviana.obtenerVertice1(), aristaLiviana.obtenerVertice2(), aristaLiviana.obtenerPeso());
			amarillos.add(aristaLiviana.obtenerVertice2());
			iterador++;
		}
		return agm;
	}

	private Arista obtenerAristaLiviana(GrafoPesado grafoPeso, Set<Integer> amarillos) {
		int pesoMinimo=-1;
		Arista aristaLiviana=null;
		
		for (Integer a: amarillos) {
			for (int j = 0; j < grafoPeso.tamano() ; j++) {
				if (!amarillos.contains(j)) {
					if(pesoMinimo==-1 || aristaLiviana.obtenerPeso()>=grafoPeso.pesoArista(a, j))
					{
						pesoMinimo=grafoPeso.pesoArista(a, j);
						aristaLiviana = new Arista(a,j,pesoMinimo);
					}
				}
			}
		}
		
		return aristaLiviana;
	}	
	
	
	
}
