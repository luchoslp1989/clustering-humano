package grafos;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

public class BFSTest {

	@Test (expected = IllegalArgumentException.class)
	public void testNull() 
	{
		BFS.esConexo(null);
	}
	
	public void vacioTest()
	{
		GrafoPesado g = new GrafoPesado(0);
		assertTrue(BFS.esConexo(g));
	}
	
	@Test
	public void alcanzablesTest()
	{
		GrafoPesado g = inicializarGrafo();
		Set<Integer> alcanzables = BFS.alcanzables(g, 0);
		int[] esperados = {0, 1, 2, 3};
		
		Assert.Iguales(esperados, alcanzables);
	}
	
	@Test
	public void alcanzables3Test()
	{
		GrafoPesado grafo = new GrafoPesado(3);
		grafo.agregarArista(0, 1, 7);
		grafo.agregarArista(0, 2, 1);
		grafo.agregarArista(1, 2, 6);
		AGM agm= new AGM();
		GrafoPesado obtenido= agm.arbolGenerador(grafo);
		
		Set<Integer> alcanzables = BFS.alcanzables(obtenido, 0);
		int[] esperados = {0, 1, 2};
		
		Assert.Iguales(esperados, alcanzables);
		
		
		
	}
	
	@Test
	public void conexoTest()
	{
		GrafoPesado g = inicializarGrafo();
		g.agregarArista(3, 4,1);
	
		assertTrue(BFS.esConexo(g));
	}
	
	@Test
	public void noConexoTest()
	{
		GrafoPesado g = inicializarGrafo();
		
		assertFalse(BFS.esConexo(g));
		
	}
	
	private GrafoPesado inicializarGrafo() 
	{
		GrafoPesado grafo = new GrafoPesado(5);
		grafo.agregarArista(0, 1,1);
		grafo.agregarArista(0, 2,1);
		grafo.agregarArista(2, 3,1);
		return grafo;
	}

}
